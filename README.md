# Test-Meli-Description

Descripción solución de reto de mutantes mercado libre.

**Índice**

1. [Consideraciones](#id1)
2. [Herramientas](#id2)
3. [Arquitectura](#id3)
4. [Repositorios](#id4)
5. [Consumo de Servicios](#id5)
6. [Licencia](#id6)


## Consideraciones<a name="id1"></a>

- Se plantea una solución en la nube.
- Para evitar sobreescribir conteos se usan operaciones atómicas.
- Se hace uso de servicios serverless para ahorro de costos de infraestructura y autoescalamiento automático en función del consumo de servicios. 
- Se plantea un flujo de integración continuo de código para verificar la calidad y las pruebas. 
- Se plantea un flujo de despliegue continuo de código para cambio de versiones de forma automatizada. 



## Herramientas<a name="id2"></a>

- Lenguaje de programación Typescript (NodeJS).
- Framework de pruebas Jasmine.
- Herramienta para aprovisionamiento de infraestructura en la nube Terraform.
- Nube para implementación de solución AWS.
- Servicio de automatización de integración y despliegue de código Gitlab.
- Especificación de apigateway OpenApi 3.0.

## Arquitectura<a name="id3"></a>

Se plantea una arquitectura en la nube diseñada para alta concurrencia y variaciones en la cantidad de peticiones. La arquitectura se despliega en la nube de aws con servicios serverless, consta de 1 api gateway,  3 funciones lambda y dos tablas de dynamo.  A continuación se presenta el diagrama de dicha arquitectura. 

![](docs/MeliArchitecture.jpg)

Inicialmente se consume el servicio ***/mutant*** para identificar mutantes y humanos el servicio retorna código 200 si es mutante y 403 si es humano. El servicio lo expone el apigateway **meli-pro-main-api** y llama a la función lambda **meli-pro-is-mutant**,  esta función cuenta con el algoritmo para identificar si es mutante y guarda el registro en la base de datos **meli-pro-records-table**. Cada vez que se crea uno o más registros en la base de datos meli-pro-records-table se ejecuta la función **meli-pro-create-stats-mutant**, esta función incrementa de forma atómica el contador de humanos o mutantes dependiendo del caso en la tabla **meli-pro-stats-table** . Finalmente se expone el servicio ***/stats*** que retorna la cuenta actual de los mutantes y humanos. El servicio lo expone el apigateway **meli-pro-main-api** y llama a la función lambda **meli-pro-is-mutant**, esta función hace una consulta del conteo de mutantes y humanos en la tabla **meli-pro-stats-table**.

## Repositorios<a name="id4"></a>

La solución planteada se implementa haciendo uso de 4 repositorios, 3 para las lambdas y apigateway y 1 para el aprovisionamiento de infraestructura. Cada repositorio cuenta con su pipeline para el despliegue e integración continua. 

|                                         **Nombre**                                        |                                                                                                               **Descripción**                                                                                                              | **Último Pipeline** | **Cobertura** |
|:-----------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------------:|---------------|
|    [Test-Meli-Lambda-Is-Mutant](https://gitlab.com/nickdpz/test-meli-lambda-is-mutant)    | Función lambda que identifica si un adn corresponde a un mutante<br>o un humano y guarda en la tabla de registro. La función<br>lambda es llamada por un ApiGW, la definición se encuentra<br>en el archivo api-docs.json del repositorio. |      [Éxito](https://gitlab.com/nickdpz/test-meli-lambda-is-mutant/-/pipelines/432904950)      |     97.72     |
| [Test-Meli-Lambda-Create-Stats](https://gitlab.com/nickdpz/test-meli-lambda-create-stats) | Función lambda que incrementa el conteo de mutantes o humanos <br>en la tabla de estadísticas a partir del evento de "PutItem"<br>de la tabla de registros.                                                                                |      [Éxito](https://gitlab.com/nickdpz/test-meli-lambda-create-stats/-/pipelines/432903609)      |      100      |
|    [Test-Meli-Lambda-Get-Stats](https://gitlab.com/nickdpz/test-meli-lambda-get-stats)    | Función lambda que entrega el conteo actualizado de mutantes, <br>humanos y el radio de mutantes entre el total. La función lambda<br>es llamada por un ApiGW, la definición se encuentra en el archivo<br>api-docs.json del repositorio.  |      [Éxito](https://gitlab.com/nickdpz/test-meli-lambda-get-stats/-/pipelines/432906551)      |      100      |
|               [Test-Meli-Infra](https://gitlab.com/nickdpz/test-meli-infra)               | Infraestructura de solución en la nube de aws. Desarrollada con <br>terraform.                                                                                                                                                             |      [Éxito](https://gitlab.com/nickdpz/test-meli-infra/-/pipelines/432897525)      |       NA      |

## Consumo de Servicios<a name="id5"></a>


| **Objectivo** | **Metodo** | **Endpoint** | **Definción** |
|---------------|:----------:|:------------:|:-------------:|
|   Se expone un servicio que verifica el ADN y guarda en base de datos.                   |    POST    |https://4gxvefyr7g.execute-api.us-east-2.amazonaws.com/test/mutant|https://gitlab.com/nickdpz/test-meli-lambda-is-mutant/-/blob/main/api-docs.json#L145|
|  Se expone un servicio que retorna las estadísticas de la cantidad de mutantes y humanos.|    POST    |https://4gxvefyr7g.execute-api.us-east-2.amazonaws.com/test/stats |https://gitlab.com/nickdpz/test-meli-lambda-get-stats/-/blob/main/api-docs.json#L145|



## Licencia<a name="id6"></a>
GNU AFFERO GENERAL PUBLIC LICENSE
